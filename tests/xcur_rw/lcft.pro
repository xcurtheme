TEMPLATE = app
TARGET = lcft

QT += gui
CONFIG += qt console warn_on
#CONFIG += debug_and_release
#CONFIG += debug
CONFIG += release

CONFIG += link_pkgconfig
PKGCONFIG += xcursor
#PKGCONFIG += xfixes


QMAKE_LFLAGS_RELEASE += -s


DESTDIR = .
OBJECTS_DIR = _build/obj
UI_DIR = _build/uic
MOC_DIR = _build/moc
RCC_DIR = _build/rcc


include($$PWD/../../src/xcr/xcrimg.pri)


INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD


HEADERS += \
  $$PWD/main.h \


SOURCES += \
  $$PWD/main.cpp \
