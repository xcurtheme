/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QDebug>
//#include <QtCore>

#include "main.h"

#include <QApplication>
#include <QStringList>
#include <QTextCodec>
#include <QTextStream>

#include <QDir>


#include "xcrimg.h"
#include "xcrxcur.h"
#include "xcrtheme.h"


///////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
  QTextCodec::setCodecForCStrings(QTextCodec::codecForName("koi8-r"));
  QTextCodec::setCodecForLocale(QTextCodec::codecForName("koi8-r"));

  QApplication app(argc, argv);

#if 0
  QDir d("/home/ketmar/.icons/dragon_of_the_fire/cursors/");
  //XCursorImages il(d, "X_cursor");
  XCursorImages *il = new XCursorImagesXCur(d, "center_ptr");
  qDebug() << il->count();

  // dump images
  {
    QDir d(".");
    d.mkdir("_dump");
    for (int f = 0; f < il->count(); f++) {
      const XCursorImage *img = il->at(f);
      qDebug() << img->name();
      img->image().save("./_dump/"+img->name()+".png");
    }
    // now dump the script

    QFile fl("./_dump/"+il->name()+".xcg");
    if (fl.open(QIODevice::WriteOnly)) {
      QTextStream stream;
      stream.setDevice(&fl);
      stream.setCodec("UTF-8");
      for (int f = 0; f < il->count(); f++) {
        const XCursorImage *img = il->at(f);
        stream << "1 " << img->xhot() << " " << img->yhot() << " "+img->name()+".png" << " " << img->delay() << "\n";
      }
      fl.close();
    }
    //1 1 1 center_ptr_0.png 50
  }

  {
    QByteArray ba(il->genXCursor());
    QFile fo("center_ptr");
    if (fo.open(QIODevice::WriteOnly)) {
      fo.write(ba);
      fo.close();
    }
  }

  {
    il->setTitle("Fire Dragon");
    il->setAuthor("Sleeping Dragon");
    il->setSite("http://sleeping-dragon.deviantart.com/art/Fire-Dragon-30419542");
    il->setDescr("Fire Dragon Cursor Theme -- part of the 'Four Dragons' suite");
    QByteArray ba(il->genXCursor());
    QFile fo("center_ptr_dsc");
    if (fo.open(QIODevice::WriteOnly)) {
      fo.write(ba);
      fo.close();
    }
  }

  delete il;
#endif

/*
  const char **nlst = nameTransTbl;
  while (1) {
    if (!nlst[0] && !nlst[1]) break;
    if (!nlst[0]) qDebug() << "***";
    else {
      if (strlen(*nlst) < 2) qDebug() << "idx:" << (int)(**nlst);
      else qDebug() << "[" << nlst[0] << "]";
    }
    nlst++;
  }
  return 0;
*/

  QDir d("/home/ketmar/.icons/dragon_of_the_fire");
  XCursorTheme *ti = new XCursorTheme(d, "fire_dragon");

/*
  ti->setTitle("Fire Dragon");
  ti->setAuthor("Sleeping Dragon");
  ti->setSite("http://sleeping-dragon.deviantart.com/art/Fire-Dragon-30419542");
  ti->setDescr("Fire Dragon Cursor Theme -- part of the 'Four Dragons' suite");
  ti->fixInfoFields();
*/

  QDir dd(".");
  dd.mkdir("fire_dragon");
  dd.cd("fire_dragon");
  ti->writeToDir(dd);

  delete ti;

  return 0;
}
