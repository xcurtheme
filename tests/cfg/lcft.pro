TEMPLATE = app
TARGET = lcft

QT += gui
CONFIG += qt console warn_on
#CONFIG += debug_and_release
#CONFIG += debug
CONFIG += release

#QMAKE_LFLAGS_RELEASE += -s


DESTDIR = .
OBJECTS_DIR = _build/obj
UI_DIR = _build/uic
MOC_DIR = _build/moc
RCC_DIR = _build/rcc


INCLUDEPATH += $$PWD $$PWD/../../src
DEPENDPATH += $$PWD $$PWD/../../src

HEADERS += \
  $$PWD/../../src/cfgfile.h \
  $$PWD/main.h \


SOURCES += \
  $$PWD/../../src/cfgfile.cpp \
  $$PWD/main.cpp \
