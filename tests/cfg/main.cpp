/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QDebug>
//#include <QtCore>

#include "main.h"

#include <QApplication>
#include <QStringList>
#include <QTextCodec>
#include <QTextStream>


#include "cfgfile.h"


///////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
  QTextCodec::setCodecForCStrings(QTextCodec::codecForName("koi8-r"));
  QTextCodec::setCodecForLocale(QTextCodec::codecForName("koi8-r"));

  QApplication app(argc, argv);

  QMultiMap<QString, QString> cfg = loadCfgFile("index.theme", true);
  QStringList keys(cfg.keys());
  keys.removeDuplicates();
  foreach (const QString &key, keys) {
    qDebug() << key;
    foreach (const QString &v, cfg.values(key)) {
      qDebug() << " " << v;
    }
  }
}
