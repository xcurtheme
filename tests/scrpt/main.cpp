/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QDebug>
//#include <QtCore>

#include "main.h"

#include <QApplication>
#include <QList>
#include <QVector>
#include <QRegExp>
#include <QStringList>
#include <QTextCodec>
#include <QTextStream>


static const QString script = "1-30,1\r\n30,99999999999999";


static bool str2num (const QString &s, quint32 &res) {
  quint64 n = 0;
  if (s.isEmpty()) return false;
  for (int f = 0; f < s.length(); f++) {
    QChar ch = s.at(f);
    if (!ch.isDigit()) return false;
    n = n*10+ch.unicode()-'0';
  }
  //if (n >= (quint64)0x100000000LL) n = 0xffffffffLL;
  if (n >= (quint64)0x80000000LL) n = 0x7fffffffLL;
  res = n;
  return true;
}


typedef struct {
  quint32 from, to;
  quint32 delay;
} tAnimSeq;


QList<tAnimSeq> parseScript (const QString &script, quint32 maxFrame) {
  QList<tAnimSeq> res;
  QString scp = script; scp.replace("\r", "\n");
  QStringList scpL = scp.split('\n', QString::SkipEmptyParts);
  foreach (QString s, scpL) {
    s = s.simplified();
    //qDebug() << s;
    QStringList fld = s.split(',', QString::SkipEmptyParts); //BUG!BUG!BUG!
    if (fld.size() != 2) {
     qDebug() << "script error:" << s;
      qWarning() << "script error:" << s;
      continue;
    }
    // frame[s]
    int hyph = fld[0].indexOf('-');
    tAnimSeq a;
    if (hyph == -1) {
      // just a number
      if (!str2num(fld[0], a.from)) {
       qDebug() << "script error (frame):" << s;
        qWarning() << "script error (frame):" << s;
        continue;
      }
      a.from = qMax(qMin(maxFrame, a.from), (quint32)1)-1;
      a.to = a.from;
    } else {
      // a..b
      if (!str2num(fld[0].left(hyph), a.from)) {
       qDebug() << "script error (frame from):" << s;
        qWarning() << "script error (frame from):" << s;
        continue;
      }
      a.from = qMax(qMin(maxFrame, a.from), (quint32)1)-1;
      if (!str2num(fld[0].mid(hyph+1), a.to)) {
       qDebug() << "script error (frame to):" << s;
        qWarning() << "script error (frame to):" << s;
        continue;
      }
      a.to = qMax(qMin(maxFrame, a.to), (quint32)1)-1;
    }
    // delay
    if (!str2num(fld[1], a.delay)) {
     qDebug() << "script error (delay):" << s;
      qWarning() << "script error (delay):" << s;
      continue;
    }
    if (a.delay < 10) a.delay = 10;
    qDebug() << "from" << a.from << "to" << a.to << "delay" << a.delay;
    res << a;
  }
  return res;
}


///////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
  QTextCodec::setCodecForCStrings(QTextCodec::codecForName("koi8-r"));
  QTextCodec::setCodecForLocale(QTextCodec::codecForName("koi8-r"));

  QApplication app(argc, argv);

  QList<tAnimSeq> sq = parseScript(script, 50);
  foreach (const tAnimSeq &a, sq) {
    qDebug() << "*from" << a.from << "to" << a.to << "delay" << a.delay;
  }

  return 0;
}
