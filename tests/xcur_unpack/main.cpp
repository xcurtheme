/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QDebug>
//#include <QtCore>

#include "main.h"

#include <QApplication>
#include <QStringList>
#include <QTextCodec>
#include <QTextStream>

#include <QDir>


#include "xcrimg.h"
#include "xcrxcur.h"


///////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
  QTextCodec::setCodecForCStrings(QTextCodec::codecForName("koi8-r"));
  QTextCodec::setCodecForLocale(QTextCodec::codecForName("koi8-r"));

  QApplication app(argc, argv);

  QStringList args(QCoreApplication::arguments());
  if (args.size() < 2) {
    fprintf(stderr, "usage: %s infile [outdir]\n", argv[0]);
    return 1;
  }

  XCursorImages *il = new XCursorImagesXCur(args.at(1));
  qDebug() << il->count() << "cursors found";

  QString dirS(".");
  if (args.size() > 2) dirS = args.at(2);
  if (!dirS.isEmpty() && !dirS.endsWith('/')) dirS += '/';

  QDir d;
  d.mkdir(dirS);

  // dump images
  {
    for (int f = 0; f < il->count(); ++f) {
      const XCursorImage *img = il->at(f);
      qDebug() << img->name();
      img->image().save(dirS+img->name()+".png");
    }
    // now dump the script

    QFile fl(dirS+il->name()+".xcg");
    if (fl.open(QIODevice::WriteOnly)) {
      QTextStream stream;
      stream.setDevice(&fl);
      stream.setCodec("UTF-8");
      stream << "; nothing xhot yhot pic delay\n";
      for (int f = 0; f < il->count(); ++f) {
        const XCursorImage *img = il->at(f);
        stream << "1 " << img->xhot() << " " << img->yhot() << " "+img->name()+".png" << " " << img->delay() << "\n";
      }
      fl.close();
    }
    //1 1 1 center_ptr_0.png 50
  }

  delete il;

  return 0;
}
