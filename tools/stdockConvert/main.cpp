/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <QDebug>
//#include <QtCore>

#include "main.h"

#include <QApplication>
#include <QStringList>
#include <QTextCodec>
#include <QTextStream>

#include <QDir>


#include "xcrimg.h"
#include "xcrxcur.h"
#include "xcrtheme.h"
#include "xcrthemefx.h"
#include "xcrthemexp.h"


///////////////////////////////////////////////////////////////////////////////
static XCursorTheme *loadTheme (const QString &fname) {
  XCursorTheme *ct = 0;
  //
  if (fname.endsWith(".CursorFX", Qt::CaseInsensitive)) {
    ct = new XCursorThemeFX(fname);
  } else if (fname.endsWith(".CurXPTheme", Qt::CaseInsensitive)) {
    ct = new XCursorThemeXP(fname);
  }
  //
  if (ct && !ct->count()) { delete ct; ct = 0; }
  return ct;
}


static void fixTextCodec () {
  QTextCodec *kc;
  const char *ll = getenv("LANG");
  //
  if (!ll || !ll[0]) ll = getenv("LC_CTYPE");
  if (ll && ll[0] && (strcasestr(ll, "utf-8") || strcasestr(ll, "utf8"))) return;
  kc = QTextCodec::codecForName("koi8-r");
  if (!kc) return;
  QTextCodec::setCodecForCStrings(kc);
  QTextCodec::setCodecForLocale(kc);
}


///////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
  fixTextCodec();

  QApplication app(argc, argv);

  bool doPack = false, doRemove = false;
  int argNo = 1;

  while (argNo < argc) {
    if (!strcmp(argv[argNo], "-p")) doPack = true;
    else if (!strcmp(argv[argNo], "-P")) doPack = doRemove = true;
    else break;
    ++argNo;
  }

  if (argNo >= argc) {
    fprintf(stderr,
      "usage: %s [-p] [-P] file [file...]\n"
      " -p: pack converted theme\n"
      " -P: pack  converted theme and remove source\n"
      "",
      argv[0]);
    return 1;
  }

  QStringList flist;

  while (argNo < argc) {
    QString fn(argv[argNo++]);
    QFileInfo fi(fn);
    if (fi.exists() && fi.isReadable()) flist << fn;
  }
  flist.removeDuplicates();

  foreach (const QString &fn, flist) {
    XCursorTheme *ct = loadTheme(fn);
    if (!ct) continue;

    //FIXME: use /tmp ?
    QString outFName(fn);
    outFName.truncate(outFName.lastIndexOf('.'));

    // write theme
    QDir dd(".");
    dd.mkdir(outFName);
    if (dd.cd(outFName)) {
      ct->writeToDir(dd);
      dd.cd("..");
      // pack theme?
      if (doPack) {
        if (!packXCursorTheme(outFName+".tgz", dd, outFName, doRemove)) {
          fprintf(stderr, "ERROR: can't pack theme %s!\n", outFName.toLocal8Bit().constData());
        } else {
          printf("theme %s.tgz sucessfully created.\n", outFName.toLocal8Bit().constData());
        }
      }
    }
    delete ct;
  }

  return 0;
}
