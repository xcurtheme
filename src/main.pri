DEPENDPATH += $$PWD
INCLUDEPATH += $$PWD

QT += gui


include($$PWD/xcr/xcrimg.pri)


HEADERS += \
  $$PWD/cfgfile.h \
  $$PWD/crtheme.h \
  $$PWD/previewwidget.h \
  $$PWD/itemdelegate.h \
  $$PWD/thememodel.h \
  $$PWD/selectwnd.h \
  $$PWD/main.h \


SOURCES += \
  $$PWD/cfgfile.cpp \
  $$PWD/crtheme.cpp \
  $$PWD/previewwidget.cpp \
  $$PWD/itemdelegate.cpp \
  $$PWD/thememodel.cpp \
  $$PWD/selectwnd.cpp \
  $$PWD/main.cpp \


FORMS += \
  $$PWD/ui/selectwnd.ui \
