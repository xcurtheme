DEPENDPATH += $$PWD
INCLUDEPATH += $$PWD

QT += gui


HEADERS += \
  $$PWD/xcrimg.h \
  $$PWD/xcrxcur.h \
  $$PWD/xcrtheme.h \
  $$PWD/xcrthemefx.h \
  $$PWD/xcrthemexp.h \


SOURCES += \
  $$PWD/xcrimg.cpp \
  $$PWD/xcrxcur.cpp \
  $$PWD/xcrtheme.cpp \
  $$PWD/xcrthemefx.cpp \
  $$PWD/xcrthemexp.cpp \
