/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
//#include <QtCore>
#include <QDebug>

#include "main.h"

#include <QApplication>
#include <QFile>
#include <QImage>
#include <QString>
#include <QStringList>
#include <QTextCodec>
#include <QTextStream>


///////////////////////////////////////////////////////////////////////////////
static void fixTextCodec () {
  QTextCodec *kc;
  const char *ll = getenv("LANG");
  //
  if (!ll || !ll[0]) ll = getenv("LC_CTYPE");
  if (ll && ll[0] && (strcasestr(ll, "utf-8") || strcasestr(ll, "utf8"))) return;
  kc = QTextCodec::codecForName("koi8-r");
  if (!kc) return;
  QTextCodec::setCodecForCStrings(kc);
  QTextCodec::setCodecForLocale(kc);
}


///////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
  fixTextCodec();

  QApplication app(argc, argv);

  //qDebug() << findDefaultTheme() << getCurrentTheme();

  SelectWnd *sw = new SelectWnd;
  sw->show();
  sw->setCurrent();
  return app.exec();
}
